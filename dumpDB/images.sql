-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2023-05-01 04:33:58
-- サーバのバージョン： 10.4.27-MariaDB
-- PHP のバージョン: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `pizza_db`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `images`
--

CREATE TABLE `images` (
  `id` bigint(20) NOT NULL,
  `create_dt` datetime DEFAULT NULL,
  `image_code` varchar(255) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `album_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- テーブルのデータのダンプ `images`
--

INSERT INTO `images` (`id`, `create_dt`, `image_code`, `image_name`, `link`, `note`, `album_id`) VALUES
(1, '2023-05-01 00:00:00', 'IM01', 'Fuji at night', '\'https://en.wikipedia.org/wiki/Mount_Fuji', 'Fuji Moutain', 1),
(2, '2023-04-29 00:00:00', 'FUJI02', 'Fuji with Sakura', 'https://en.wikipedia.org/wiki/Mount_Fuji', 'In spring', 1),
(3, '2023-04-29 00:00:00', 'JINJA01', 'Jinja in Spring', 'https://en.wikipedia.org/wiki/Shinto_shrine', 'In spring', 2),
(4, '2023-04-29 00:00:00', 'JINJA02', 'Shinto_shrine', 'https://en.wikipedia.org/wiki/Shinto_shrine', 'In winter', 2),
(5, '2023-04-29 00:00:00', 'FOREST01', 'Aokigahara', 'https://en.wikipedia.org/wiki/Aokigahara', 'In spring', 3),
(6, '2023-04-29 00:00:00', 'VN01', 'Langbian', 'https://vi.wikipedia.org/wiki/N%C3%BAi_Langbiang', 'In spring', 4);

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_niop0u4v6svl8sblkt7ulxp4b` (`image_code`),
  ADD KEY `FK724cv7ds8vwsp7mpi6e5s7keq` (`album_id`);

--
-- ダンプしたテーブルの AUTO_INCREMENT
--

--
-- テーブルの AUTO_INCREMENT `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- ダンプしたテーブルの制約
--

--
-- テーブルの制約 `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK724cv7ds8vwsp7mpi6e5s7keq` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
