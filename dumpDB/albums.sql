-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2023-05-01 04:34:17
-- サーバのバージョン： 10.4.27-MariaDB
-- PHP のバージョン: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `pizza_db`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `albums`
--

CREATE TABLE `albums` (
  `id` bigint(20) NOT NULL,
  `album_code` varchar(255) NOT NULL,
  `album_name` varchar(255) NOT NULL,
  `create_dt` datetime DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- テーブルのデータのダンプ `albums`
--

INSERT INTO `albums` (`id`, `album_code`, `album_name`, `create_dt`, `note`) VALUES
(1, 'ALB01', 'Mountain Pictures', '2023-04-29 00:00:00', 'Pictures of Fuji Mountain'),
(2, 'ALB02', 'Japan Jinja', '2023-04-29 00:00:00', '66 Kumano Jinja Images'),
(3, 'ALB03', 'Japan Forest Pictures', '2023-04-29 00:00:00', 'The Japanese jungle of Ishigaki, Yaeyama Islands'),
(4, 'ALB04', 'VietNam Mountain', '2023-04-29 00:00:00', 'Pictures of mountain all over Vietnam'),
(5, 'AB05', 'Test', '2023-04-30 23:56:50', '');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_h7tyk8kn08kwun4pk76r2dy11` (`album_code`);

--
-- ダンプしたテーブルの AUTO_INCREMENT
--

--
-- テーブルの AUTO_INCREMENT `albums`
--
ALTER TABLE `albums`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
