package com.devcamp.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.backend.model.CAlbum;

public interface IAlbumRepository extends JpaRepository<CAlbum, Long>{
    CAlbum findImageByAlbumCode(String albumCode);
    Optional<CAlbum> findByAlbumCode(String albumCode);
    Optional<CAlbum> findById(Long id);
}
