package com.devcamp.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.backend.model.CImage;

public interface IImageRepository extends JpaRepository<CImage, Long> {
    Optional<CImage> findByImageCode(String imageCode);
    Optional<CImage>findById(Long id);
}
