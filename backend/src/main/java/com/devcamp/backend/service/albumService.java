package com.devcamp.backend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.backend.model.CAlbum;
import com.devcamp.backend.model.CImage;
import com.devcamp.backend.repository.IAlbumRepository;

@Service
public class albumService {
    @Autowired
    IAlbumRepository pAlbumRepository;
    //get all
    public ArrayList<CAlbum> getAllAlbums() {
        ArrayList<CAlbum> listAlbums = new ArrayList<>();
        pAlbumRepository.findAll().forEach(listAlbums::add);
        return listAlbums;
    }

    //get Image by album code
    public List<CImage> getImageByAlbumCode(String albumCode) { 
        CAlbum vAlbum = pAlbumRepository.findImageByAlbumCode(albumCode);
        if (vAlbum != null) {
            return  vAlbum.getImages();
        } else {
            return null;
        }
    }

    //get Album by id
    public CAlbum getAlbumById(Long id) {
        Optional<CAlbum> vAlbum = pAlbumRepository.findById(id);
        if (vAlbum != null) {
            return vAlbum.get();
        } else
            return null;
    }

    
}
