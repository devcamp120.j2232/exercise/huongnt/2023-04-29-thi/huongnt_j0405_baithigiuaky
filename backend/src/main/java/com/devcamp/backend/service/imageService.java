package com.devcamp.backend.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.devcamp.backend.model.CImage;
import com.devcamp.backend.repository.IImageRepository;

@Service
public class imageService {
    @Autowired
    IImageRepository pImageRepository;

    // get all image
    public ArrayList<CImage> getAllImages() {
        ArrayList<CImage> listImages = new ArrayList<>();
        pImageRepository.findAll().forEach(listImages::add);
        return listImages;
    }

    // get image by id
    public CImage getImageById(Long id) {
        Optional<CImage> vImage = pImageRepository.findById(id);
        if (vImage != null) {
            return vImage.get();
        } else
            return null;
    }

}
