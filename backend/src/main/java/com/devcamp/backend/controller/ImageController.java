package com.devcamp.backend.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.devcamp.backend.model.CAlbum;
import com.devcamp.backend.model.CImage;
import com.devcamp.backend.repository.IAlbumRepository;
import com.devcamp.backend.repository.IImageRepository;
import com.devcamp.backend.service.imageService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ImageController {
    @Autowired
    imageService pImageService;

    // get all image list with service
    @GetMapping("/images")
    public ResponseEntity<List<CImage>> getAllImagesApi() {
        try {
            return new ResponseEntity<>(pImageService.getAllImages(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    IImageRepository pImageRepository;

    // get album by ID with service
    @GetMapping("/images/details/{id}")
    public CImage getImageById(@PathVariable long id) {
        return pImageService.getImageById(id);
    }

    // GET image by image code
    // truyền vào mã hình để lấy ra đối tượng hình (VD truyền vào: FUJI01)
    @GetMapping("/images/{imagecode}")
    public ResponseEntity<Object> getCImageByImageCode(@Valid @PathVariable("imagecode") String imageCode) {
        try {
            Optional<CImage> imageData = pImageRepository.findByImageCode(imageCode);
            // Todo: viết code lấy voucher theo id tại đây
            if (imageData.isPresent()) {
                return new ResponseEntity<Object>(imageData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>("Image code not found", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<Object>("Image code not found", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create new image without service
    // create new image with album ID
    // tạo image cho từng album ID tương ứng
    @Autowired
    private IAlbumRepository pAlbumRepository;

    @PostMapping("/images/create/{id}")
    public ResponseEntity<Object> createImage(@Valid @PathVariable("id") Long id, @RequestBody CImage cImage) {
        try {
            Optional<CAlbum> albumData = pAlbumRepository.findById(id);
            if (albumData.isPresent()) {
                CImage newImage = new CImage();
                newImage.setImageCode(cImage.getImageCode());
                newImage.setImageName(cImage.getImageName());
                newImage.setLink(cImage.getLink());
                newImage.setNote(cImage.getNote());
                newImage.setCreatedAt(new Date());

                CAlbum _album = albumData.get();
                newImage.setAlbum(_album);
                // newImage.setAlbumName(_album.getAlbumName());

                CImage savedRole = pImageRepository.save(newImage);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // update image by id
    @PutMapping("/images/update/{id}")
    public ResponseEntity<Object> updateImage(@Valid @PathVariable("id") Long id, @RequestBody CImage cImage) {
        Optional<CImage> imageData = pImageRepository.findById(id);
        if (imageData.isPresent()) {
            CImage newImage = imageData.get();
            newImage.setImageCode(cImage.getImageCode());
            newImage.setImageName(cImage.getImageName());
            newImage.setLink(cImage.getLink());
            newImage.setNote(cImage.getNote());
            CImage savedImage = pImageRepository.save(newImage);
            return new ResponseEntity<Object>(savedImage, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>("Image does not exist", HttpStatus.NOT_FOUND);
        }
    }

    // delete image by id
    @DeleteMapping("/images/delete/{id}")
    public ResponseEntity<Object> deleteImageById(@Valid @PathVariable Long id) {
        try {
            Optional<CImage> optional = pImageRepository.findById(id);
            if (optional.isPresent()) {
                pImageRepository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<Object>("Not found", HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<Object>("Failed to Delete Image", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
