package com.devcamp.backend.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.backend.model.CAlbum;
import com.devcamp.backend.model.CImage;
import com.devcamp.backend.repository.IAlbumRepository;
import com.devcamp.backend.service.albumService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AlbumController {
    @Autowired
    private albumService pAlbumService;

    // get all album list with service
    @GetMapping("/albums")
    public ResponseEntity<List<CAlbum>> getAllAlbumsApi() {
        try {
            return new ResponseEntity<>(pAlbumService.getAllAlbums(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @Autowired
    private IAlbumRepository pAlbumRepository;

    //get album by ID
    @GetMapping("/albums/details/{id}")
    public CAlbum getAlbumById(@PathVariable long id) {
        return pAlbumService.getAlbumById(id);
    }


    // GET album by album code
    //truyền vào mã album để lấy ra đối tượng album (VD truyền vào: ALB01)
    @GetMapping("/albums/{albumcode}")
    public ResponseEntity<Object> getCAlbumByAlbumCode(@Valid @PathVariable("albumcode") String albumCode) {
        try {
            Optional<CAlbum> albumData = pAlbumRepository.findByAlbumCode(albumCode);
            // Todo: viết code lấy voucher theo id tại đây
            if (albumData.isPresent()) {
                return new ResponseEntity<Object>(albumData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>("Album code not found", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<Object>("Album code not found", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get image by album code
    //truyền vào mã album (VD truyền vào: ALB01)
    @GetMapping("/albums/image")
    public ResponseEntity<Object> getImageByAlbumCode(@RequestParam(value = "albumCode") String albumCode) {
        try {
            List<CImage> images = pAlbumService.getImageByAlbumCode(albumCode);
            if (images != null) {
                return new ResponseEntity<Object>(images, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>("album code not found", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<Object>("Fail to get image by album code", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create album without service
    @PostMapping("/albums/create")
    public ResponseEntity<Object> createAlbum(@Valid @RequestBody CAlbum cAlbum) {
        try {
            CAlbum album = new CAlbum();
            album.setAlbumCode(cAlbum.getAlbumCode());
            album.setAlbumName(cAlbum.getAlbumName());
            album.setNote(cAlbum.getNote());
            album.setCreatedAt(new Date());
            album.setImages(cAlbum.getImages());
            CAlbum savedAlbum = pAlbumRepository.save(album);
            return new ResponseEntity<>(savedAlbum, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Album: " + e.getCause().getCause().getMessage());
        }
    }

    // update album by id without service
    @PutMapping("/albums/update/{id}")
    public ResponseEntity<Object> updateCountry(@Valid @PathVariable("id") Long id, @RequestBody CAlbum cAlbum) {
        Optional<CAlbum> albumData = pAlbumRepository.findById(id);
        if (albumData.isPresent()) {
            CAlbum newAlbum = albumData.get();
            newAlbum.setAlbumCode(cAlbum.getAlbumCode());
            newAlbum.setAlbumName(cAlbum.getAlbumName());
            newAlbum.setNote(cAlbum.getNote());
            // newAlbum.setImages(cAlbum.getImages());
            CAlbum savedAlbum = pAlbumRepository.save(newAlbum);
            return new ResponseEntity<Object>(savedAlbum, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>("Album does not exist", HttpStatus.NOT_FOUND);
        }
    }

    // delete album by id without service
    @DeleteMapping("/albums/delete/{id}")
    public ResponseEntity<Object> deleteAlbumById(@Valid @PathVariable Long id) {
        try {
            Optional<CAlbum> optional = pAlbumRepository.findById(id);
            if (optional.isPresent()) {
                pAlbumRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<Object>("Not found", HttpStatus.NOT_FOUND);
            }
      
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<Object>("Failed to Delete Album", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
