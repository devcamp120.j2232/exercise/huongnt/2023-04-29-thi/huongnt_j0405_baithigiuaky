package com.devcamp.backend.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name = "albums")
public class CAlbum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Please input Album Code")
    @Size(min = 2, message = "Album Code must be at least 2 characters ")
    @Column(name = "album_code", unique = true)
    private String albumCode;  // mã album

    @NotNull(message = "Please input Album Name")
    @Column(name = "album_name")
    private String albumName; //tên album

    @Column(name = "note")
    private String note; //mô tả

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;

    
    @OneToMany(targetEntity = CImage.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "album_id")
    private List<CImage> images;

    public CAlbum() {
    }

    public CAlbum(Long id, String albumCode, String albumName, String note, Date createdAt,List<CImage> images) {
        this.id = id;
        this.albumCode = albumCode;
        this.albumName = albumName;
        this.note = note;
        this.createdAt = createdAt;
        this.images = images;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlbumCode() {
        return albumCode;
    }

    public void setAlbumCode(String albumCode) {
        this.albumCode = albumCode;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public List<CImage> getImages() {
        return images;
    }

    public void setImages(List<CImage> images) {
        this.images = images;
    }
   
    

    
    
}
