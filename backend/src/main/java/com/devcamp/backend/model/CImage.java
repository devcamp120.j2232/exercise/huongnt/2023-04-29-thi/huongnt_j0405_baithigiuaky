package com.devcamp.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "images")
public class CImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Please input Image Code")
    @Size(min = 2, message = "Image Code must be at least 2 characters ")
    @Column(name = "image_code", unique = true)  
    private String imageCode;  //mã hình

    
    @NotNull(message = "Please input Image Name")
    @Column(name = "image_name")
    private String imageName; //tên hình

    @Column(name = "link")
    private String link;  //link hình

    @Column(name = "note")
    private String note;  //mô tả

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;

    
    @ManyToOne
    @JsonIgnore
    private CAlbum album;

    public CImage() {
    }



    public CImage(Long id, String imageCode,String imageName, String link, String note, Date createdAt,
            CAlbum album) {
        this.id = id;
        this.imageCode = imageCode;
        this.imageName = imageName;
        this.link = link;
        this.note = note;
        this.createdAt = createdAt;
        this.album = album;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public CAlbum getAlbum() {
        return album;
    }

    public void setAlbum(CAlbum album) {
        this.album = album;
    }



    public String getNote() {
        return note;
    }



    public void setNote(String note) {
        this.note = note;
    }

   
    
    
}
